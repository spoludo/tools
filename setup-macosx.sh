# for mac os x...
## install brew, the package manager
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
## install luarocks, the Lua package manager
brew install luarocks
## install LDoc, the Lua doc generator
luarocks install penlight
luarocks install LDoc

