local OutStream = require 'spoludo.dsl.OutStream'
local Writer = require 'spoludo.dsl.Writer'

jpa = {}

function jpa:Entity(package)
    return function (name)
        local g = Writer(OutStream(package:gsub('%.','/')..'/'..name..'.java', 'w'))
        g.context.entity = name
        return function (cols)
            g:write('package ',package,';\n',
                    '\n',
                    'import javax.persistence.Entity;\n',
                    'import javax.persistence.GeneratedValue;\n',
                    'import javax.persistence.GenerationType;\n',
                    'import javax.persistence.Id;\n',
                    '\n',
                    '@Entity\n',
                    '@Table(name="',name:upper(),'")\n',
                    'public class ',name,' implements Serializable {\n',
                    '\n')
             :indent()
             :    write('private static final long serialVersionUID = 1L;\n',
                          '\n')
                          
                  -- declare the columns
             :    apply(cols, 'decl')
            
                  -- default constructor
             :    write(name,'() {\n',
                        '}\n\n')
                    
                  -- initialization constructor
             :    write('public ',name,'(\n')
             :    indent()
                      -- list the columns
             :        reduce(
                              -- align the type declaration
                        g:map(cols, 0, function(col, acc)
                                           return tostring(col.type)..' ', acc
                                       end,
                                       g.calc_alignment, 
                                       g.align_right, 
                                       
                              -- add the names to the types       
                              cols, '', function(col,foo) 
                                            return col.name, foo
                                        end),
                                        
                        -- using the prefix as the last parm has no comma
                        '', '', function(prefix,suffix)
                                   return ',\n',suffix 
                                end
                      )
             :        write(')\n')
             :    undent()
             :    write('{\n')
             :    indent()
                      -- assign the columns with aligning on the column names
             :        reduce(
                        g:map(cols, 0, function(col, acc) 
                                           return 'this.'..col.name, acc 
                                       end,
                                       g.calc_alignment, 
                                       g.align_right, 
                              cols, 0, function(col,foo) 
                                           return ' = '..col.name..';\n', foo 
                                       end)
                      )
--             :    apply(cols, function(col,g) g:write('this.',col.name,' = ',col.name,';\n') end) -- non aligned...
             :    undent()
             :    write('}\n\n')
             
                  -- generate the getters
             :    apply(cols, 
                    function(col,g)
                        g:write('public ',col.type,' get',g:change_first(col.name,'upper'),'() {\n',
                                '\treturn ',col.name,';\n',
                                '}\n\n')
                    end)
                    
                  -- generate the setters
             :    apply(cols, 'set')
                    
             :undent()
             :write('}\n')
             :get_stream():close()
        end
    end
end

local function has_setter(self,g)
    g:with{ n = self.name, t = self.type }
     :write('public void set',g:change_first(self.name,'upper'),'($t $n) {\n',
            '\tthis.$n = $n;\n',
            '}\n\n')
end

local function bypass()
end

function jpa:immutable(typename)
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@Column(name = "',g:hyphenize(self.name),'")\n')
                 :write('private ',self.type,' ',self.name,'; // immutable\n\n')
            end,
            set = bypass,
        }
    end
end

function jpa:mutable(typename)
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@Column(name = "',g:hyphenize(self.name),'")\n',
                        'private ',self.type,' ',self.name,';\n\n')
            end,
            set = has_setter,
        }
    end
end

function jpa:id(typename)
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@Id\n',
                        '@GeneratedValue(strategy = GenerationType.AUTO)\n',
                        '@Column(name = "',g:hyphenize(self.name),'")\n',
                        'private ',self.type,' ',self.name,';\n\n')
            end,
            set = has_setter,
        }
    end
end

function jpa:transient(typename)
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@Transient\n',
                        'private ',self.name,' ',self.name,';\n\n')
            end,
            set = has_setter,
        }
    end
end

function jpa:one_to_many(typename)
    return function (colname)
        return { 
            name = colname, 
            type = jpa:array(typename), 
            decl = function(self, g)
                g:write('@OneToMany(fetch = FetchType.LAZY, optional = false)\n',
                        '@JoinColumn(name = "',g.context.entity:lower(),'_id")\n',
                        'private ',self.type,' ',self.name,';\n\n')
            end,
            set = bypass,
        }
    end
end

function jpa:many_to_one(typename) 
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@ManyToOne(fetch = FetchType.LAZY, optional = false)\n',
                        '@JoinColumn(name = "',self.type:lower(),'_id")\n',
                        'private ',typename,' ',self.name,'; // immutable\n\n') 
            end,
            set = bypass, -- always immutable?
        }
    end
end


function jpa:one_to_one(typename) -- Todo
    return function (colname)
        return { 
            name = colname, 
            type = typename, 
            decl = function(self, g)
                g:write('@OneToOne(fetch = FetchType.LAZY, optional = false)\n',
                        '@JoinColumn(name = "',self.type:lower(),'_id")\n',
                        'private ',typename,' ',self.name,'; // immutable\n\n') 
            end,
            set = bypass, -- always immutable?
        }
    end
end


function jpa:array(typename)
    local mt = { __tostring = function(t) return 'List<'..t.type..'>' end }
    return setmetatable({ type = typename }, mt)
end

--
--
--


jpa:Entity('com.spoludo.cloud.challenge') 'Challenge' {
    jpa:id         ('long')   'challengeId',
    jpa:immutable  ('int')    'numPlayers',
    jpa:immutable  ('String') 'game',    
    jpa:immutable  ('String') 'setup',    
    jpa:immutable  ('Date')   'dateInitiated',  
    jpa:mutable    ('Date')   'dateStarted',   
    jpa:mutable    ('Date')   'dateFinished',    
    jpa:mutable    ('String') 'status',    
    jpa:mutable    ('int')    'currentMove',
    jpa:mutable    ('int')    'numMoves',
    jpa:transient  ('int')    'atMove',
    jpa:one_to_many('Move')   'moves',    
}

jpa:Entity('com.spoludo.cloud.challenge') 'Move' {
    jpa:id         ('long')      'moveId',
    jpa:many_to_one('Challenge') 'challenge',
    jpa:mutable    ('String')    'notation',    
    jpa:immutable  ('Date')      'dateCreated',   
    jpa:mutable    ('Date')      'dateEdited',   
    jpa:immutable  ('String')    'phase',    
    jpa:immutable  ('int')       'step',    
    jpa:immutable  ('String')    'role',    
}

jpa:Entity('com.spoludo.cloud.challenge') 'Comment' {
    jpa:id         ('long')      'commentId',
    jpa:many_to_one('Challenge') 'challenge',
    jpa:mutable    ('String')    'comment',    
    jpa:immutable  ('Date')      'dateCreated',   
    jpa:mutable    ('Date')      'dateEdited',   
    jpa:mutable    ('String')    'privacy',    -- all / participants / commenter   
    jpa:immutable  ('String')    'phase',    
    jpa:immutable  ('int')       'step',    
    jpa:immutable  ('String')    'role',  -- player / non player     
}

jpa:Entity('com.spoludo.cloud.challenge') 'Commenter' {
    jpa:id         ('long')      'commenterId',
    jpa:immutable  ('String')    'username',    
    jpa:one_to_many('Move')      'comments',    
}

jpa:Entity('com.spoludo.cloud.challenge') 'Watcher' {
    jpa:id         ('long')      'watcherId',
    jpa:many_to_one('Challenge') 'challenge',
    jpa:mutable    ('String')    'username',  -- natural key, decoupling... sharding?  
    jpa:immutable  ('Boolean')   'eachMove', -- realtime/ at the end    
}

--
--
--

--
--
--

jpa:Entity('com.spoludo.cloud.account') 'Account' {
    jpa:id         ('long')       'accountId',
    jpa:immutable  ('String')     'username',    
    jpa:one_to_many('Credential') 'credentials',    
}

jpa:Entity('com.spoludo.cloud.account') 'Credential' {
    jpa:id         ('long')      'credentialId',
    jpa:many_to_one('Account')   'account',
    jpa:immutable  ('String')    'label',    
    jpa:mutable    ('Date')      'dateExpiration',   
}

--
--
--

jpa:Entity('com.spoludo.cloud.invitation') 'Invitation' {
    jpa:id         ('long')      'invitationId',
    jpa:one_to_many('Invitee')   'invitees',    
    jpa:immutable  ('String')    'game',    
}

jpa:Entity('com.spoludo.cloud.invitation') 'Invitee' {
    jpa:id         ('long')       'invitationId',
    jpa:many_to_one('Invitation') 'invitation',
    jpa:mutable    ('Boolean')    'pending',    
    jpa:mutable    ('Boolean')    'accepted',    
}

--
--
--

jpa:Entity('com.spoludo.cloud.player') 'Player' {
    jpa:id         ('long')       'playerId',
    jpa:mutable    ('String')    'username',  -- natural key, decoupling... sharding?  
}


jpa:Entity('com.spoludo.cloud.player') 'Participant' {
    jpa:id         ('long')      'participantId',
    jpa:many_to_one('Challenge') 'challenge',
}

