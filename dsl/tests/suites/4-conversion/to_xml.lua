local conversion = require 'spoludo.dsl.conversion'
local Writer = require 'spoludo.dsl.Writer'

local ast_root = dofile('tests/shared/ast_conversion_sample.lua')

print(conversion.to_xml(ast_root):get_stream())
