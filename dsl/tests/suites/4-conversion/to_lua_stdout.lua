local conversion = require 'spoludo.dsl.conversion'
local Writer = require 'spoludo.dsl.Writer'

local ast_root = dofile('tests/shared/ast_conversion_sample.lua')

conversion.to_lua(ast_root, Writer())
