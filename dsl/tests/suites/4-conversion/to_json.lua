local conversion = require 'spoludo.dsl.conversion'

local ast_root = dofile('tests/shared/ast_conversion_sample.lua')

print(conversion.to_json(ast_root):get_stream())
