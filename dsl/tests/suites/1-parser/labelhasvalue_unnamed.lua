local dsl   = require 'spoludo.dsl.dsl'

local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "label has value, unnamed", label = "ast_label", value = "ast_value" }
local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, { policy = "no label, unnamed" })
dsl.trace = dsl.trace_on

local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root {

        kw:a(0);
        kw:b(0) { foo = 0  }; 
        ka(0):c(0);
        ka(0):d(0) { foo = 0 };
        
        kw:e(0) {
            kw:a(0) ;
            kw:b(0)  { foo = 0  }; 
            ka(0):c(0) ;
            ka(0):d(0) { foo = 0 };
        }; 
    }

