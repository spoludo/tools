local dsl   = require 'spoludo.dsl.dsl'

local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "no label, named" }
local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, naming)
dsl.trace = dsl.trace_on

local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root 'r' {

        ka('a') 'a';
        kw      'b';
        ka('c') 'c' { foo = 0 };
        kw      'd' { foo = 0 };

        kw 'f' {
            ka('a') 'a';
            kw      'b';
            ka('c') 'c' { foo = 0 };
            kw      'd' { foo = 0 };
        };

    }

