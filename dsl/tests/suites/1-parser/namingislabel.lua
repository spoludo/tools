local dsl   = require 'spoludo.dsl.dsl'

local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "naming is label" }
local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, { policy = "no label, unnamed" })
dsl.trace = dsl.trace_on

local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root {

        kw:a();
        kw:b() { foo = 0 }; 
        ka(0):c();
        ka(0):d() { foo = 0 };
        
        kw:e {
            kw:a();
            kw:b() { foo = 0 }; 
            ka(0):c();
            ka(0):d {foo = 0 };
        }; 
    }

