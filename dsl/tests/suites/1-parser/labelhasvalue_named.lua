local dsl   = require 'spoludo.dsl.dsl'


local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "label has value, named", label = "ast_label", value = "ast_value" }
local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, { policy = "no label, unnamed" })
dsl.trace = dsl.trace_on

local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root {

        kw:label(1) 'a';
        kw:label(2) 'b' { foo = 0 }; 
        ka('A'):label(3) 'c';
        ka('B'):label(4) 'd' { foo = 0 };
        
        kw:label(5) 'e' {
            kw:label(6) 'a';
            kw:label(7) 'b' { foo = 0 }; 
            ka('C'):label(8) 'c';
            ka('D'):label(9) 'd' { foo = 0 };
        }; 
        
        ka('E'):label(10) 'f' {
            ka('F', 'H'):label(11) 'g' { foo = 0 };
        }; 
    }

