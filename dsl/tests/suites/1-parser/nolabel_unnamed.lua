local dsl   = require 'spoludo.dsl.dsl'

local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "no label, unnamed" }
local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, naming)
dsl.trace = dsl.trace_on

local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root  {

        ka('a');
        kw();
        ka('c') { foo = 0 };
        kw  { foo = 0 };

        kw  {
            ka('a') ;
            kw      ;
            ka('c') { foo = 0 };
            kw      { foo = 0 };
        };

    }

