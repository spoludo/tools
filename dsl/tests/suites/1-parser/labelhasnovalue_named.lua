local dsl   = require 'spoludo.dsl.dsl'

local annotations   = { policy = "", defs = { "ast_annot" } }
local noannotations = { policy = "no annotations" }
local naming = { policy = "label has no value, named", label = "ast_label" }

local testdsl = {}
dsl.make_rule(testdsl, 'kw', noannotations, naming)
dsl.make_rule(testdsl, 'ka', annotations, naming)
dsl.make_rule(testdsl, 'root',noannotations, { policy = "no label, unnamed" })
local root, kw, ka = testdsl.root, testdsl.kw, testdsl.ka

return root {

        kw:label 'b' {}; 
        kw:label 'a';
        ka(0):label 'c';
        ka(0):label 'd' {};

        kw:label 'e' {
            kw:label 'b' {}; 
            kw:label 'a';
            ka(0):label 'c';
            ka(0):label 'd' {};
        };

    }

