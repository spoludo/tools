local testcase = assert(arg[1])
local ok,_,suite = testcase:find('.*%/([^%/]+)%/[^%/]+$')
assert(ok, 'invalid testcase '..testcase)
dofile(testcase)
local switch = {
	["1-parser"] = function()
		print('check')
	end,

	["2-ast"] = function()
		print('check')
	end,

	["3-writer"] = function()
		print('check')
	end,

	["4-conversion"] = function()
		print('check')
	end,

	["5-symbol"] = function()
		print('check')
	end,

}
switch[suite]()

--[[
return function(fn)

    function dump(str, t, ind, sep, visited)
        ind = ind or "    "
        sep = sep or "}\n"
        visited = visited or {}
        assert(type(t) == "table" and not visited[t])
        visited[t] = true
        str = str .. '{\n'
        function writekey(k)
            return type(k) == "string" and k or '['..k..']'
        end
        for k,v in pairs(t) do
            assert(type(k) == "number" or type(k) == "string")
            if type(v) == "table" then
                str = str .. ind .. writekey(k) .. ' = '
                str = dump(str, v, ind.."    ",ind..'},\n',visited)
            elseif type(v) == "string" then
                str = str .. ind .. writekey(k) .. ' = ' .. string.format("%q", v) .. ',\n'
            else 
                assert(type(v) ~= "function" and type(v) ~= "coroutine")
                str = str .. ind .. writekey(k) .. ' = ' .. tostring(v) .. ',\n'
            end
        end
        str = str .. sep
        return str
    end
    
    function equals(ta, tb)
        if type(ta) ~= "table" or type(tb) ~= "table" then return false end
        -- all tb keys are in ta?
        for k in pairs(tb) do        
            if not ta[k] then return false end
        end
        -- same values for the keys?
        for k,v in pairs(ta) do
            if type(v) == "table" then
                if not equals(v, tb[k]) then return false end            
            elseif v ~= tb[k] then
                print(k, v, tb[k])
                return false
            end
        end
        return true
    end

    local ok, nodes = pcall( fn )
    if not ok then
        print(nodes) -- contains then the error messages
        print(debug.traceback())
        print('FAILURE')
        return
    end

    local str = "return "
    str = dump(str, nodes)
    print( str )
    
    eval, err = loadstring(str) 
    if not eval then 
        print(err) 
        print("FAILURE")
    end
    local ast = eval()
    
    print(equals(ast, nodes) and "SUCCESS" or "FAILURE")
end
]]--
