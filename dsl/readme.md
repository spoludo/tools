SPOLUDO DSL
======================

Introduction
----------------------
This is a DSL (Domain Specific Library) kit in Lua.
It includes ways to define simple grammar rules that
directly maps to an Abstract Syntax Tree.

It also provides some code generation tools that helps
indenting some generation macros.

It provides a way to export the abstract syntax tree to Json,  XML and Lua.


Directories
----------------------
- src: the source code to be installed
 - spoludo: company namespace
  - dsl: the library namespace, this is what can be uninstalled  
- doc: base directory for md detail documentation, tutorial
 - generated: doc produced by LDoc from the src comments
- tests: the tests suite, tests result and test drivers
 - driver.lua, drives the tests and check the results (TBD)
 - report.lua, format the tests results
 - suites: the test case are
  - <suitname>:
   - <testcase>.lua
 - res:
  - <suitname>:
   - <testcase>.txt
  - report<date>.html


Install
----------------------

### Dependencies

This is made to have as little dependencies as possible, but should provide
some callback to bind it to a framework, such as the mkpath that can recieve
a directory creation function.

However [LDoc](https://github.com/stevedonovan/LDoc) is used for the
documentation generation. Note that the generated documentation is committed
still in the repos, so that the tools isn't needed to consume it.

### Installing it

> # put the lib in default LUA_PATH
> make install
> # generate the documentation
> make doc
> # run all the tests
> make tests

### Running a test case

> # from the project home
> # for debugging purpose
> lua tests/suites/<suitename>/<testname>.lua
> # or a quick non reg check
> lua tests/drive.lua tests/suites/<suitename>/<testname>.lua


Examples
----------------------
Not much so far, so the tests are what can give a clue.
