Todo: no roadmap as it need prioritization
====================

Test
--------------------
- unit test writer: currently more an example than a unit test
- unit test symbol
- unit test ast
- unit test mkpath (can be mocked with create call)
- negative testing: check error cases
- code test drive and report, shall check test success

Doc
--------------------
- improve doc (build, install, dev)
- add bigger example such as a protobuf like grammar, writer jpa test
case doesn't use a full grammar as it doesn't build as ast but directly 
generates.

Build
--------------------
- making portable build 
- add luarocks packaging?

Feature
--------------------
- add more ast manipulation and ast walking (per needed)...
- allow to use ast_kind that are not string but typing function that
  can for example add a metatable to the nodes to make them class instances


