--- help exporting an AST,
-- @module conversion
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
-- @usage
-- local conversion = require('spoludo.dsl.conversion')
-- local Writer     = require('spoludo.dsl.Writer')
-- local OutStream  = require('spoludo.dsl.OutStream')
-- -- load my spec with my Domain Specific Language
-- local my_ast_root    = require('my_dsl_instance')
-- -- as a string
-- local json_str = conversion.to_json(my_ast_root, Writer(OutStream("")))
-- -- or, as it defaults to a string
-- local json_str = conversion.to_json(my_ast_root)
-- -- to stdout
-- conversion.to_xml(my_ast_root, Writer(OutStream()))

local Writer = require 'spoludo.dsl.Writer'
local OutStream = require 'spoludo.dsl.OutStream'
local ast = require 'spoludo.dsl.ast'
local conversion = {}

-- shared by the converter as it contains the normalization logic
local function walk_tree(writer, pre, t, suf, visited, to_field)
    -- the array part at first
    for i=1,#t do
    	pre, suf = to_field(writer, pre, i, t[i], suf, visited)
    end
    -- then string names: we discard ast_parent, and the node from the array part
    for k,v in pairs(t) do
        assert(type(k) == "number" or type(k) == "string")
        if type(v) == "table" then
            -- skip parent or table of symbols...
            if type(k) == "number" then
            	if not visited[v] then -- not a ast_node from the previous for
	            	-- sparse array...
	            	assert(not ast_is_node(v)) -- ast_nodes shouldn't be sparsed...
	            	pre, suf = to_field(writer, pre, k, v, suf, visited)
            	end
            elseif k ~= 'ast_parent' then
            	-- if a ast_node, check that the node was visited in the prev
		    	assert(not ast.is_node(v) or visited[k])
    			if not ast.is_node(v) then
    				assert(not visited[v]) -- forbid graphs for now
    				-- we may decide to duplicate the subtree though
    				-- provided that this is a tree from there in case
    				-- of shared table constants for example
	            	pre, suf = to_field(writer, pre, k, v, suf, visited)
    			end
    		end
    	else
        	pre, suf = to_field(writer, pre, k, v, suf, visited)
    	end
    end
    return pre, suf
end

--
-- Json
--

local to_json_impl -- forward decl

local function to_json_field(writer, pre, k, v, suf, visited)
    if type(v) == "table" then
        writer:write(pre, tostring(k), '": ')
        to_json_impl(writer,'{\n',v,'\n}',visited)
    elseif type(v) == "string" then
        writer:write(pre, tostring(k), string.format('": %q'..suf,v))
    else 
        assert(type(v) ~= "function" and type(v) ~= "coroutine")
        writer:write(pre, tostring(k), '": ',tostring(v),suf)
    end
    pre = ',\n"'
    return pre, suf
end

to_json_impl = function (writer, pre, t, suf, visited) 
    assert(type(t) == "table" and not visited[t])
    visited[t] = true
    writer:write(pre):indent()
    walk_tree(writer, '"', t, '', visited, to_json_field)
    writer:undent():write(suf)
    return writer
end

--- dump in json the content of an ast 
-- the content is normalized so that we are back to a tree
-- and not in a graph. Basically the changes from @see symbol.prepare
-- are ignored.
-- @param ast_root, the root of the abstract syntax tree
-- @param writer, a @see spoludo.dst.Writer, default to a string stream writer
-- @return the write given as a parameter
function conversion.to_json(ast_root, writer)
	assert(ast.is_node(ast_root))
	writer = writer or Writer(OutStream(""))
	return to_json_impl(writer, '{\n', ast_root, '\n}\n', {})
end

--
-- Lua
--

local to_lua_impl -- forward decl

local function to_lua_key(k)
	return type(k) == "string" and k or '['..tostring(k)..']'
end

local function to_lua_field(writer, pre, k, v, suf, visited)
    if type(v) == "table" then
        writer:write(to_lua_key(k), ' = ')
        to_lua_impl(writer,'{\n',v,'}'..suf,visited)
    elseif type(v) == "string" then
        writer:write(to_lua_key(k), string.format(' = %q'..suf,v))
    else 
        assert(type(v) ~= "function" and type(v) ~= "coroutine")
        writer:write(to_lua_key(k), ' = ',tostring(v),suf)
    end
    return pre, suf
end

to_lua_impl = function (writer, pre, t, suf, visited) 
    assert(type(t) == "table" and not visited[t])
    visited[t] = true
    writer:write(pre):indent()
    walk_tree(writer,'',t,',\n', visited, to_lua_field)
    writer:undent():write(suf)
    return writer
end


--- dump in lua the content of an ast 
-- the content is normalized so that we are back to a tree
-- and not in a graph. Basically the changes from @see symbol.prepare
-- are ignored.
-- @param ast_root, the root of the abstract syntax tree
-- @param writer, a @see spoludo.dst.Writer, default to a string stream writer
-- @return the write given as a parameter
function conversion.to_lua(ast_root, writer)
	assert(ast.is_node(ast_root))
	writer = writer or Writer(OutStream(""))
	return to_lua_impl(writer, '{\n', ast_root, "}\n", {})
end

--
-- XML
--


local to_xml_impl -- forward decl

local function to_xml_node_suf(node)
	if node.ast_name and node.ast_name ~= '' then
		return "</node> <!-- "..node.ast_name.." -->\n"
	end
	return "</node>\n"
end

local function to_xml_field(writer, pre, k, v, suf, visited)
    if type(v) == "table" then
    	if ast.is_node(v) then
	        to_xml_impl(writer,'<node>',v,to_xml_node_suf(v),visited)
    	elseif k == "ast_annotations" then
	        to_xml_impl(writer,'<annotations>\n',v,'</annotations>\n',visited)
    	else
	        writer:write('<field name="',k, '">\n'):indent()
	        to_xml_impl(writer,'<struct>\n',v,'</struct>\n',visited)
	        writer:undent():write('</field>\n')
    	end
    elseif type(v) == "string" then
    	if pre ~= '<node>' or (k ~= 'ast_loc' and k ~= 'ast_name' and k ~= 'ast_kind') then
	        writer:write('<field name="',k, '">',v,'</field>\n')
	    end
    else 
        assert(type(v) ~= "function" and type(v) ~= "coroutine")
        writer:write('<field name="',k, '">',tostring(v),'</field>\n')
    end
    return pre, suf
end

to_xml_impl = function (writer, pre, t, suf, visited) 
    assert(type(t) == "table" and not visited[t])
    visited[t] = true
    if pre == "<node>" then
	    if t.ast_name then
		    writer:write('<node ast_kind="',t.ast_kind,'" ast_name="',t.ast_name,'" ast_loc=',string.format("%q",t.ast_loc),'>\n')
		else
	    	writer:write('<node ast_kind="',t.ast_kind,'" ast_loc="',t.ast_loc,'">\n')
		end
	else
    	writer:write(pre)
	end
    writer:indent()
    walk_tree(writer,pre,t,'\n', visited, to_xml_field)
    writer:undent():write(suf)
    return writer
end

--- dump in xml the content of an ast 
-- the content is normalized so that we are back to a tree
-- and not in a graph. Basically the changes from @see symbol.prepare
-- are ignored.
-- @param ast_root, the root of the abstract syntax tree
-- @param writer, a @see spoludo.dst.Writer, default to a string stream writer
-- @return the write given as a parameter
function conversion.to_xml(ast_root, writer)
	assert(ast.is_node(ast_root))
	writer = writer or Writer(OutStream(""))
	writer:write('<?xml version="1.0" encoding="UTF-8"?>\n') -- check encoding.. describe a schema?
	return to_xml_impl(writer, "<node>",ast_root,to_xml_node_suf(ast_root),{})
end


return conversion

