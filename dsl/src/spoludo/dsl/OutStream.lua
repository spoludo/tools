--- wrap a file, a string or io.stdout to a single write contract
-- @classmod OutStream
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
-- @usage
-- local OutStream = require('spoludo.dsl.OutStream')
-- local ostrstream = OutStream("this will be prefixed") 
-- ostrstream:write(" and suffixed")
-- local str         = tostring(ostrstream)
-- ostrstream:close()
-- print(str)
-- -- shall give
-- -- this will be prefixed and suffixed
-- @usage
-- -- stdout
-- local stdoutstream = OutStream()
-- local stdoutstream = OutStream(io.stdout)
-- @usage
-- -- file descriptor
-- local fd = assert(io.open("path/to/file","w"))
-- local fdstream = OutStream(fd)
-- @usage
-- -- file name
-- local fdstream = OutStream("path/to/file","w") 

local dir = require 'spoludo.dsl.dir'

local OutStream = {
}


--- open a new stream
-- if the stream was already opened, it will be close before opening the new stream
-- if the stream includes a path, all intermediate directories will be created
-- @param resource, either a prefix string which can be empty, a path string, a file descriptor.
--        or nil in which case it will default to io.write
-- @param mode  only used for a file path where it can be <code>'w'</code>, <code>'w+'</code> or <code>'a'</code> @see io.open
-- @param basedir default <code>'./'</code>, the directoru from which the path string will be created
-- @return self the stream instance which can be <code>not is_ok()</code> if the stream has failed opening
function OutStream:open(resource, mode, basedir)
	if self.resource then self:close() end 
    resource = resource or io.write
    self.close_resource = function() end
    self.ok   = true
    self.errs = ""

    if type(resource) == "function" then
        self.resource = resource
        self.unsafe_write = function(self, ...) self.resource(...) end
        return self
    end

    if type(resource) == "string" then
        if mode == 'w' or mode == 'w+' or mode == 'a' then 
        	dir.mkpath((basedir or './')..resource, true) 
        	local errs
        	resource, errs = io.open(resource, mode) -- , print)
            self.ok = resource ~= nil
            if not self.ok then 
            	self.errs = errs
                return self
            end
            -- continue with the file descriptor
        elseif mode == nil then
            -- we have a string stream
            self.resource = resource
            self.unsafe_write = function(self, ...)
                self.resource = self.resource .. table.concat{ ... }
            end
            return self
        else
            self.ok, self.errs = false, "invalid mode for string stream"
            return self
        end
    end
    -- assuming a file descriptor... check?
    self.resource = resource    
    self.close_resource = function(self) self.resource:close() end
    self.unsafe_write = function(self, ...) self.resource:write(...) end
    return self
end

--- write to the stream
-- writes in sequence the strings in parameter. These shall have been converted by
-- the caller as number or boolean terms won't be casted to string. If the write fails, 
-- is_ok will return false, and errs then contains a error description.
-- @param ... writes a string set to the stream (only strings are accepted)
-- @return self
function OutStream:write(...)
    local parms = {...} 
    self:unsafe_write(unpack(parms))
--    self.ok, self.errs = xpcall(function() self:unsafe_write(unpack(parms)) end,
--                                debug.traceback)
    if not ok then
       io.stderr:write(self.errs)
    end
    return self
end

--- close a stream
-- after closing, the next call can only be @see OutStream:open
-- @return self
function OutStream:close()
	if self.resource then
	    self:close_resource()
	end
    self.resource = false
    self.ok       = false
    self.errs     = ""
    return self
end
--- return as a string the stream internal resource
-- this is intended to retrieve the string built by a string stream.
-- @treturn string
function OutStream:__tostring()
    return tostring(self.resource)
    -- make tostring a read* for the file descriptor case?
end

--- tells if the stream is working or has reached an error state
-- @treturn boolean true is stream is fine
function OutStream:is_ok()
    return self.ok
end

--- tells and describes the error state of the stream
-- @return false when no error was detected
-- @return true, error string when some errors were raised
function OutStream:has_err()
    return not self.ok, self.errs
end

setmetatable(OutStream, OutStream)
OutStream.__index = OutStream

--- constructor
-- create a stream and opens it. If the opening fails, @see is_ok is set to false
-- and @see has_err describes the error.
--
-- @param resource @see OutStream.open
-- @param mode     @see OutStream.open
-- @param basedir  @see OutStream.open
-- @return self
function OutStream:__call(resource, mode, basedir) 
    local stream = {
	    ok = false,
	    resource = false,
	    errs = "",
	    close_resource = function() end,
	    unsafe_write = function() end
    }
    setmetatable(stream, self)
    return stream:open(resource or io.write, mode, basedir)
end

return OutStream
