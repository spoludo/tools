--- help manipulating an AST,
-- @module ast
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
-- @usage
-- local ast         = require('spoludo.dsl.ast')
-- -- load my specs with my Domain Specific Language
-- local my_ast_root1   = require('my_dsl_instance1')
-- local my_ast_root2   = require('my_dsl_instance2')
-- ensure we have node
-- assert(ast.is_node(my_ast_node1) and ast.is_node(my_ast_node1))
-- -- are they duplicates?
-- print(ast.equals(my_ast_root1, my_ast_root2))
-- ast_inject_parent(my_ast_root1)
-- assert(my_ast_root1[1].ast_parent == my_ast_root1)
-- do they still compare the same?
-- print(ast.equals(my_ast_root1, my_ast_root2))
local ast = {}

--- deeply compares two sub trees to check they are both the same AST
-- one shall not have called @see symbol.prepare on one of the table only
-- thus either both or none! The reason is that prepare adds fields to the 
-- node table, thus we don't have the same structure if only substree has 
-- this added fields.
-- one may have call ast.inject_parent on of the table (ignoring ast_parent
-- for the comparison)
-- the function is intended for unit test and non regression test.
-- @param ta a ast_node
-- @param tb a ast_node
-- @treturn boolean true if they have the same shape
function ast.equals(ta, tb)
    if type(ta) ~= "table" or type(tb) ~= "table" then return false end
    -- all tb keys are in ta?
    for k in pairs(tb) do        
        if not ta[k] and k ~= "ast_parent" then return false end
    end
    -- same values for the keys?
    for k,v in pairs(ta) do
        if type(v) == "table" then
            if not equals(v, tb[k]) then return false end            
        elseif v ~= tb[k] then
            print(k, v, tb[k])
            return false
        end
    end
    return true
end

       
--- check that the parameter is a node
-- there is no strong typing of the node
-- thus it relies upon ensuring we have a table having a ast_loc field
-- @param candidate, hopefully a ast_node
-- @return false if not a ast_node or the ast_node
function ast.is_node(candidate)
    return (type(candidate) == 'table' and candidate.ast_loc) and candidate or false
end

--- add the parent relation to the nodes
-- This shall be called from the root node of the AST
-- there is unlikely a need to call it directly though, as @see symbol.prepare
-- calls it
-- @param ast_node, the node whose children will it is their parent
function ast.inject_parent(ast_node)
    assert(ast.is_node(ast_node))
    for c=1,#ast_node do
        if is_node(ast_node[c]) then
            ast_node[c].ast_parent = ast_node
            ast.inject_parent(ast_node[c])
        end
    end
end

return ast
