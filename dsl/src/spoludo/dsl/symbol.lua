--- help coping with the symbols of an AST,
-- @module symbol
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
-- @usage
-- local symbol         = require('spoludo.dsl.symbol')
-- -- load my spec with my Domain Specific Language
-- local my_ast_root    = require('my_dsl_instance')
-- -- load the predefined names in the DSL that can be references
-- local builtins = require('my_dsl_builtin_names')
-- -- inject the ast_parent file in a node and 
-- -- index the node children by their name in their ast_parent node
-- symbol.prepare(my_ast_root)
-- -- now we can resolve some symbols
-- local typedef_node = symbols:resolve(
--     my_ast_root.MyClass.MyArrayT,
--     my_ast_root.MyClass.MyArrayT.of, 
--     builtins
-- )
-- -- or print some full name
-- print(symbol.fullname(typedef_node))

local ast = require 'spoludo.dsl.ast'

local symbol = {
}

--- get the full name of an abstract syntax tree node,
-- that is the path from the tree root to the node.
-- The precondition is that the AST table of symbol has been build with calling.<br>
-- <code>
-- fullname ::= id [ '.' id ]* <br>
-- id       ::= name | '[' number ']'
-- </code>
-- @see symbol.prepare
-- @tparam table ast_node
-- @treturn string the fullname
function symbol.fullname(ast_node)
    assert(ast.is_node(ast_node))
    assert(ast_node.ast_parent, "need to build the table of symbols before calling ast.make_fullname")
    local name, parent = ast_node.ast_name,  ast_node.ast_parent
    if name then
        if parent then
            name = symbol.fullname(ast_node.ast_parent)..'.'..name
        end
    elseif parent then
        local parent 
        for c=1,#parent do
            if parent[c] == ast_node then
                name = symbol.fullname(ast_node.ast_parent)..'['..tostring(c)..']'
                return name
            end
        end
    else
        name = "[0]" -- better convention?
    end
    return name
end


local function build_table_of_symbols(ast_node, errs)
    for c=1,#ast_node do
        if is_node(ast_node[c]) and ast_node[c].ast_name then
            if ast_node[ast_node[c].ast_name] then
                if is_node(ast_node[ast_node[c].ast_name]) then
                    errs = (errs or "") .. "[ERR] "..ast_node[c].ast_name.."duplicated in "..ast_node[c].ast_loc.." from "..ast_node[ast_node[c].ast_name].ast_loc
                else
                    errs = (errs or "") .. "[ERR] "..ast_node[c].ast_name.."duplicated in "..ast_node[c].ast_loc
                end
            end
            ast_node[ast_node[c].ast_name] = ast_node[c]
            errs = build_table_of_symbols(ast_node[c], errs)
        end
    end
    return errs
end

--- prepare the abstract syntax tree navigation
-- nodes are added a ast_parent relation
-- and a child by name relation
-- @usage
-- a = node 'a' {
--     node 'b' {
--     }
-- }
-- a = {                           a = {
--    ast_name = 'a',                 ast_name = 'a',
--    [1] = {                         b = [1] = {
--                         ===>          ast_parent = a,
--       ast_name = 'b',                 ast_name = 'b',
--    }                               }
-- }                               }
-- @tparam  table ast_root, base of the syntax tree
-- @treturn table the ast_root given as a parameter
function symbol.prepare(ast_root)
    ast.inject_parent(ast_root)
    local errs = build_table_of_symbols(ast_root)
    assert(not errs, errs)
    return ast_root
end

local function tokenize_ref(ref)
	local path = { }
	repeat
	    -- find '[' <number> ']' from the begining
		local b, e, i = reference:find('^%[(%d+)%]')
		if b then
			path[#path+1] = tonumber(i)
			reference = reference:sub(e+1)
		else
		    -- find longuest name without '.' or '[' from the beginning
		    b, e, l = reference:find('^([^%.%[]+)')
		    if b then
		    	path[#path+1] = l		    
		    	local next_char = reference:sub(e+1,e+1)
		    	if #next_char == 0 then
		    		reference = ""
		    	elseif next_char == "[" then
		    		reference = reference:sub(e+1)
		    	else -- skip '.'
			    	reference = reference:sub(e+2)
			    end
		    else
		    	return nil -- invalid ref
		    end
		end
	until reference == ""
	return path
end



--- find the ast node referenced by a ref or a builtin construct
-- @tparam table scope, that is the ast_node that contains the reference
-- @tparam string ref, the reference to resolve
-- @tparam table builtins, a map name and custom node table or boolean set to true
-- @treturn table the resolved table or nil if the reference is dangling
function symbol.resolve(scope, ref, builtins)
	assert(ast.is_node(scope) and type(ref) == "string")
    local tokens = tokenize_ref(ref)
    if tokens then
	    -- now we can start searching the reference from the parent scope
	    while scope do
	    	local down = scope
		    for i=1,#tokens do
		    	local token = tokens[i]
				down = down[token] 
				if not down or not ast.is_node(down) then
					down = nil
					break
				end
		    end
		    if down then
		    	return scope
		    else
			    scope = scope.ast_parent
			end
		end
		if #path == 1 and builtins[path[1]] then
			return builtins[path[1]]
		end
	end
    return nil
end


return symbol
