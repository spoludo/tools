--- directory manipulation primitives
-- @module dir
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014

local dir = {}

--- take a filename and extract the path with creating it or not
-- avoid depending of lfs or apr, but bof bof with os.execute, assuming sep is /
-- and no error check
-- @param filename whose path shall be created
-- @param create shall we only extract the path or create it in the file system.
--        <ul><li>when false or nil, extract only</li>
--            <li>when true create it in the file system with os.execute</li>
--            <li>when a function, applies the function for each dir in the path</li></ul>
-- @tparam function trace print the constructed path to the stream (nil or print)
-- @treturn table eg <code>'./.././../a/./b/c/d/.././f.txt'<br>-><br>{<br>
--prefix = '../../',<br> path = 'a/b/c',<br>filename = 'f.txt';<br>'a', 'b', 'c'<br>}</code>
-- @usage
-- -- get file info
-- local fileinfo = dir.mkpath('./.././../a/./b/c/d/.././f.txt')
-- @usage
-- -- create the path with os.execute
-- dir.mkpath('./.././../a/./b/c/d/.././f.txt', true)
-- @usage
-- -- trace the path extraction
-- dir.mkpath('./.././../a/./b/c/d/.././f.txt', false, print)
function dir.mkpath(filename, create, trace) 
	local split = {}
	if not trace then 
		trace = function() end
	end
	create = create 
	         and 
	             (type(create) == 'function' 
	                  and
	                  	create
	                  or
	                  	function(dirname) 
	                  		local d,e = io.open(dirname, 'r')
	                  		if not d then 
	                  			os.execute("mkdir " .. dirname)
	                  		else
	                  			d:close()
	                  		end
	                  	end)
	         or function(foo) end	                  
	-- create the needed dirs
	local _,_,path,filename = filename:find('(.*)%/([^%/]*)')
	path = path or ''
	trace(path, filename)
	split.filename = filename
	-- make path canonical
	path = path:reverse():gsub('%/%.%.%/[^%/%.]+',''):reverse()
	trace(path)
	path = path:gsub('%/%.%/','%/'):gsub('^%.%/',''):gsub('%/%.$','')
	trace(path)
	local has_prefix,_,prefix = path:find('^([%.%/]+)')
	if  has_prefix then
		path = path:sub(#prefix+1)
	else
		prefix = '.'
	end
	trace('mkdir', prefix, '/', path)
	split.prefix = prefix
	split.path   = path
	local creating = io.open(prefix..'/'..path, 'r')
	if creating then
		-- already exists, wont recreate it
		creating:close()
		creating = nil
	else
		creating = prefix
	end
	for dirname in path:gmatch('[^%/]+') do
		if creating then
			creating = creating .. "/" .. dirname
			trace(prefix, creating)
			create( creating )
		end
		split[#split+1] = dirname
	end
	trace('done')
	return split
end

return dir
