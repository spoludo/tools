--- functions to create a Domain Specific Language parser
-- @module dsl
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
local dsl = { }

function dsl.no_trace() end

function dsl.trace_on(...)  
    io.stderr:write(table.concat({...}, ' '), '\n') 
end

dsl.trace = dsl.no_trace

--- retrieve the file info for the ast node
-- this helps having meaningful error messages or assert in thecode
-- @tparam number upstack the depth of the stack between the user model and the
--                        rule procesing (generally 2)
-- @treturn string "filename":line
function dsl.where(upstack)
    local loc = debug.getinfo((upstack or 0)+1, "Sl")
    return string.format('"%s":%d', loc.short_src, loc.currentline)
end

--- parsing of a rule keyword(annot) { opt_body }
-- this will use the opt_body or infer a table when one shift
-- to create a ast node, that is call () on the return table.
-- the shift is done for children node, but has to be explicit at the root node.
-- This is why it is not recommeded accepting an empty root.
-- <pre><code>{
--     ast_kind = 'keyword',
--     ast_loc  = '"file":100',
--     ast_annotations = { annot }
--     opt_body
-- }</code></pre>
-- typically one will have a 
-- function keyword(...) return dsl.parse_opt_body(dsl.where(2), 'keyword', nil, ...) end
-- for a keyword such as func, calling parse body means we are at the cursor ^
-- when calling parse body
-- <pre><code>
-- func  { }
--     ^
-- func  ;
--     ^
-- </code></pre>
--
-- @tparam string loc location of the parsed statement, @see dsl.where
-- @tparam string kind ast_kind of the node, usually the keyword used for the rule
-- @tparam string name ast_name optional name of the node 
-- @param  ... some annotations parameters that will be stored in <code>ast_annotations = { key = value }</code>
-- @return complex ;-) basically a parsing function, that returns a node

function dsl.parse_opt_body(loc, kind, name, ...)
    local annotations = { ... }
    dsl.trace('parsing',kind,name)
    
    -- the body is optional, if it is not used like with ';' instead of {}
    -- the following function will be returned instead of a node
    -- calling it though from the outer scope will infer a node in place of it
    return function(body)
        dsl.trace('parsing',kind,name,'body')
        -- no declared body, was a ';' so we must create the ast node
        if type(body) == "nil" then
            dsl.trace('no body: inferring',kind,name)
            body = { }
        end
        assert(type(body) == 'table', 'expecting table, got '..type(body))

        -- check the declarations inside the body, one may have
        for i=1,#body do
            -- the child node doesn't exist, we still have this
            -- parsing function closure
            if type(body[i]) == "function" then
                dsl.trace('shift')
                -- call the function closure so that it infer the node like above
                body[i] = body[i]()
            end
        end
        
        -- add the ast node specific data inside the body...
        assert(body.ast_name == nil and body.ast_kind == nil and body.ast_loc == nil  and body.ast_annotations == nil) -- downgrade to error?
        body.ast_name = name
        body.ast_kind = kind
        body.ast_loc  = loc
        
        -- annotations are a sequence of name,value 
        body.ast_annotations = #annotations > 0 and {} or nil 
        for i=1,#annotations,2 do
            assert(body[annotations[i]] == nil, "already annoted with "..annotations[i]) -- downgrade to warning?
            body.ast_annotations[annotations[i]] = annotations[i+1]
        end
        
        dsl.trace('PARSED / reduce', kind, name)
        
        -- pollute env for the root node... assuming there is only on instance of its kind
        --if kind == root then 
        --    assert(type(_G[name]) == 'nil', name..' already present in the global scope')
        --    _G[name]=body 
        --end
        
        return body
    end
end

--- parsing of a rule keyword(annot) 'name' { opt_body }
-- this will use the opt_body or infer a table to create a ast node
-- we take benefit of the literal call syntax suger from Lua to avoid
-- the parenthesis of the call around the 'name'
-- <pre><code>{
--     ast_kind = 'keyword',
--     ast_name = 'name',
--     ast_loc  = '"file":100',
--     ast_annotations = { annot }
--     opt_body
-- }</code></pre>
-- typically one will have a 
-- function keyword(...) return dsl.parse_name(dsl.where(2), 'keyword', ...) end
-- for a keyword such as func, calling parse name means we are at the cursor ^
-- when calling parse name
-- <pre><code>
-- func 'name' { }
--     ^
-- </code></pre>
--
-- @tparam string loc location of the parsed statement, @see dsl.where
-- @tparam string kind ast_kind of the node, usually the keyword used for the rule
-- @param  ... some annotations parameters that will be stored in <code>ast_annotations = { key = value }</code>
-- @return complex ;-) basically a name parsing function, that returns a body parsing function that returns a node
function dsl.parse_name(loc, kind, ...)
    dsl.trace('parse_name',kind)
    local annotations = { ... }
    
    return function(name)
        assert(type(name) == 'string', 'string waited, got '..type(name)..' '..tostring(name))
        return dsl.parse_opt_body(loc, kind, name, unpack(annotations))
    end
end

--- creating rules for the dsl grammar
-- a rule is building a ast node
-- a gammar rule can be like this:
-- <code>
-- rule    ::= keyword [ '(' annotations ')' ] name ( body | ';' )<br>
-- rule    ::= keyword [ '(' annotations ')' ] ':' label [ name ( body | ';' ) ]<br>
-- rule    ::= keyword [ '(' annotations ')' ] ':' label '(' value ')' [ name ( body | ';' ) ]<br>
-- keyword ::=  </code>a valid Lua identifier<br><code>
-- name    ::=  </code>a Lua string, preferably a quoted identifier<br><code>
-- label   ::=  </code>a valid Lua identifier, either undefined or enforced by the dsl
--             it can be undefined in the case of an enum, or enforced for a built in type
--             of the dsl<br>
-- </code>
-- @usage
-- local dsl = require'spoludo.dsl.dsl'
-- mydsl = {}
-- local annots  = { policy = { 'annot_tag' } }
-- local naming = { policy = 'no label, named', label = 'annot_tag' }
-- dsl.make_rule(mydsl,'enum',annot,naming )
-- -- using the rule                               -- annotation policy
-- mydsl.enum(0) 'a';                              no label, named
-- @usage
-- local dsl = require'spoludo.dsl.dsl'
-- mydsl = {}
-- local annots  = { 'annot_tag' }
-- local naming  = { policy = 'label has value, unnamed', label = 'annot_tag' }
-- -- using the rule                               -- annotation policy
-- mydsl.enum:a(0);                                label has value, unnamed
-- dsl.make_rule(mydsl,'enum',annot,naming )
-- @usage
-- -- using the rule                               -- annotation policy
-- mydsl.enum(0) 'a';                              no label, named
-- mydsl.enum:a(0);                                label has value, unnamed
-- mydsl.enum(0):a;                                naming is label
-- mydsl.field:string 'a';                         label has no value, named
-- mydsl.field:array'int' 'b';                     label has value, named
-- mydsl.If ( mydsl.eq('a', mydsl.const'A') ) { }  no label, unnamed
-- -- on may alias the rules to make it lighter
-- local enum, field, eq, const = mydsl.enum, mydsl.field, mydsl.eq, mydsl.const
-- but note that If can confict with a reserved keyword if not capitalized.
-- @param dslud       parser for the dsl under definition
-- @param keyword     name of the rule
-- @param annotations { policy = "" or "" or ""
--                      
--                    }
-- @param naming      { policy = cf table above
--                      label  = '' key in the ast node of the label
--                      value  = '' key in the ast node for the label value if any
--                    }
-- @return dslud
function dsl.make_rule(dslud, keyword, annotations, naming)
-- make it OO? myDSL extends dsl with constructor self:make_rule() ...
    local switch = {
        ["no label, named"] =
            function(name) -- 
                local loc = dsl.where(2)
                return dsl.parse_opt_body(loc, keyword, name)
            end,
        ["no label, unnamed"] =
            function(body) --
                local loc = dsl.where(2)
                return dsl.parse_opt_body(loc, keyword, '')(body)
            end,
        ["label has value, unnamed"] =
            function(self,label) -- 
                local loc = dsl.where(2)
                return function(self, value)
                    return dsl.parse_opt_body(loc, keyword, '', naming.label, label, naming.value, value, unpack(self.annotations))
                end
            end,
        ["naming is label"] =
            function(self, name) -- 
                local loc = dsl.where(2)
                return function(self)
                    return dsl.parse_opt_body(loc, keyword, name, unpack(self.annotations))
                end
            end,
        ["label has no value, named"] =
            function(self,label) -- 
                local loc = dsl.where(2)
                return function (self, name)
                    return dsl.parse_opt_body(loc, keyword, name, naming.label, label, unpack(self.annotations))
                end
            end,
        ["label has value, named"] =
            function(self,label) -- 
                local loc = dsl.where(2)
                return function(self, value) 
                    dsl.trace("value", naming.value, tostring(value))
                    return dsl.parse_name(loc, keyword, naming.label, label, naming.value, value, unpack(self.annotations))
                end
            end,
    }
    
    local function build_annotations(defs,...)
        local annots =  {}
        for i=1,select('#',...) do
            local label, value = defs[i], select(i,...)
            if not label then
                -- if unknown label, use the value as the label
                -- and set value to true
                assert(type(value)=="string", "unknown annotation kind not a string"..tostring(value))
                label, value = label or value, label and value or true -- one line matters!!!
            end
            annots[#annots+1] = label
            annots[#annots+1] = value
            dsl.trace("annotation", label, tostring(value))
        end
        return annots
    end
    
    local parser   
    if naming.policy == "no label, unnamed" or naming.policy == "no label, named" then
        if annotations.policy ~= "no annotations" then
            parser = 
                function (...)
                    local annots =  build_annotations(annotations.defs, ...)
                    if naming.policy == "no label, unnamed" then
                        return function(body) --
                            local loc = dsl.where(2)
                            return dsl.parse_opt_body(loc, keyword, '', unpack(annots))(body)
                        end
                    end
                    return function(name) -- 
                        local loc = dsl.where(2)
                        return dsl.parse_opt_body(loc, keyword, name, unpack(annots))
                    end
                end
        else
            parser = switch[naming.policy]
        end
    else
        parser = { annotations = {} }
        parser.__index = switch[naming.policy] 
        
        if annotations.policy ~= "no annotations" then
            
            parser.annotations_defs = annotations.defs 
            
            parser.__call =
                function(self, ...)
                    local annots =  build_annotations(self.annotations_defs, ...)
                    self.annotations = annots
                    return self 
                end
                
        end
        setmetatable(parser, parser)
    end
    
    dslud[keyword] = parser
    
    return dslud
end

-- next thing could be constraints on rules, ie what node are accepted in
-- a parent node as child. But this could be in ast as well with a checking pass
-- another constraint could be on the annotations

return dsl
