--- helper class to generate indented code,
-- @classmod Writer
-- @author thierry@spoludo.com
-- @license MIT
-- @copyright Spoludo ou 2014
-- @usage
-- local Writer = require('spoludo.dsl.Writer')
-- 

local OutStream = require('spoludo.dsl.OutStream')

local Writer = {}

local function expand (s, vars)
	local delim = vars.delim or '$'
    s = string.gsub(s, delim..'(%w+)', function (n)
        return vars[n] or n
    end)
    return s
end

--- add an indentation level
-- all future generated lines will be prefixed by this indentation level
-- @return self
function Writer:indent() 
    self.indentation = self.indentation + 1                
    return self
end

--- remove an indentation level
-- all future generated lines will be prefixed by this indentation level
-- @return self
function Writer:undent()
    assert(self.indentation > 0)
    self.indentation = self.indentation - 1
    return self
end

--- remove an indentation level
-- write an indented code block with expansing some eventual macro variables
-- previously defined with a call to @see Writer.with
-- @param ... a sequential list of text (just like @see io.write) 
-- @return self
function Writer:write(...)
    local template = ''
    local nargs = select('#',...) 
    local vars  = self.vars or {}
    if type(select(nargs,...)) == 'table' then
        for k,v in select(nargs,...) do
            vars[k] = tostring(v)                    
        end
        nargs = nargs - 1
    end
    for i=1,nargs do
        template = template .. tostring(select(i,...))
    end
    if #template > 0 then
        local prefix = self.rep(self.indentation_step, self.indentation)
        if self.offset > #prefix then
            prefix = prefix .. self.rep(' ',#prefix - self.offset)
        end
        template = template:gsub('\t',self.indentation_step)
        template = template:gsub('\n(.)', '\n'..prefix..'%1')
        template = expand(template, vars)
        self.stream:write(self.lastline == '' and template ~= '\n' and prefix or '',
                          template)
        self.vars = {}
        local lastnewline, last = template:find('^.*()\n')
        if lastnewline then
            self.lastline = template:sub(last+1)
        else -- no new lines...
            self.lastline = template
        end
    end
    return self
end

--- defines some variable value that will be expanded in the next write call
-- @usage
-- writer:with({ a = 'v', b = 'w', delim = '@' }):write('@a+@b')
-- <pre><samp>
-- v + w
-- </samp></pre>
-- @usage
-- writer:with({ a = 'v', b = 'w', delim = '$' }):write('$a$a+$b$c')
-- <pre><samp>
-- vv+w$c
-- </samp></pre>
-- @tparam table vars with contains pairs (var name var value) and an eventual
--               delimiter if $ can't be used
function Writer:with(vars)
    self.vars = vars
    return self
end

--- applies a serie of transforms feeded by an init state on a 2d list of code 
-- This can be used to format align series of ast node such as commented typed
-- parameters. The list can then be concatened vertically with the @see Writer.reduce
-- @usage
-- <pre><samp>
-- </samp></pre>
-- @param list      a list of code block with the same number of lines
-- @param init      init parameter for the transformation
-- @param transform 1st operator applied, that return a modifier init for the 
--                  next transform
-- @param ...       optional further transforms that takes the modified list
-- @return the transformed list
function Writer:map(list, init, transform, ...)
    local tomerge = {}
    local t, last, n = 1, select('#',...), #list
    while list do
        local acc, transformed = init, {}
        for i=1,#list do            
            transformed[i] = list[i]
        end
        list = nil
        while transform do
            for i=1,#transformed do            
                transformed[i], acc = transform(transformed[i], acc)
            end
            transform = nil
            if t > last then
                break
            elseif type(select(t, ...)) == "function" then
                transform = select(t, ...)
                t = t + 1
            else
                list = select(t, ...)
                t = t + 1
                assert(type(list) == "table" and #list == n)
                break
            end
        end
        tomerge[#tomerge+1] = transformed
        if list then
            while t <= last do
                init = select(t, ...)
                t = t + 1
                if type(init) == "table" then
                    tomerge[#tomerge+1] = list
                    list = init
                else
                    assert(type(init) ~= "function" and t <= last and type(select(t, ...)) == "function")
                    transform = select(t, ...)
                    t = t + 1
                    break
                end
            end
        end
    end
    return tomerge
end

--- write 2d concatenation of a code blocks list
-- The concatenation can prefix and suffix each lines of the assembled code blocks
-- with the given suffix and prefix. A transform operator can be used to modify
-- these prefix and suffix after each lines
-- @param tomerge generally the output of @see Writer.map
-- @param prefix optional empty by default prefix for a line 
--               can be a comma+newline for lists with no comma for the last item, 
--               seeded then with an empty prefix that will be mutated by the transform
-- @param suffix optional empty by default prefix for a line
--               can be a semicolon for a lists of statements
-- @param transform a prefix,suffix modifer function that is the identity function by default.
-- @return self
function Writer:reduce(tomerge, prefix, suffix, transform)
    local result = tomerge[1]
    prefix = prefix or ''
    suffix = suffix or ''
    transform = transform or function(a,b) return a,b end
    for l=1,#result do
        self:write(prefix, result[l])
        for i=2,#tomerge do
            self:write(tomerge[i][l])
        end
        self:write(suffix)
        prefix, suffix = transform(prefix, suffix)
    end
    return self
end

--- transform operator that returns the max length of a line
-- the line is not expanded by vars or mutated by the indentation
-- intended to be used with @see Writer.map
-- @tparam string line to measure
-- @tparam number maxsize the previous max size
-- @return the unmodified line, the new max size
function Writer.calc_alignment(line, maxsize)
    return line, maxsize < #line and #line or maxsize
end

--- transform operator that aligns a string to left 
-- the line is not expanded by vars or mutated by the indentation
-- intended to be used with @see Writer.map
-- the alignment shall have been calculated with @see Writer.calc_alignment
-- @tparam string line to align
-- @tparam number alignment the previous max size
-- @return the aligned line, the alignment
function Writer.align_left(line, alignment)
    return string.rep(' ', alignment - #line) .. line, alignment 
end

--- transform operator that aligns a string to right 
-- the line is not expanded by vars or mutated by the indentation
-- intended to be used with @see Writer.map
-- the alignment shall have been calculated with @see Writer.calc_alignment
-- @tparam string line to align
-- @tparam number alignment the previous max size
-- @return the aligned line, the alignment
function Writer.align_right(line, alignment)
    return line .. string.rep(' ', alignment - #line), alignment 
end

--- keep the current line size as the new indentation
-- the call can not be nested, only pairs mark() unmark()
-- @return self
function Writer:mark()
    self.offset = #self.lastline 
    return self
end

--- cancel the mark, to resume with normal indentation 
-- @ see Writer.mark()
-- @return self
function Writer:unmark()
    self.offset = 0
end

--- cut a pattern inside a text 
-- be aware the the cutting is from left to right, use @see string.reverse
-- and a reverted to pattern if you want from right to left.
-- @tparam string text to modify
-- @tparam string tocut pattern
-- @treturn string the modified text without the pattern instances
function Writer:cut(text, tocut)
    return text:gsub(tocut, '')
end

--- camelize an identifier 
-- remove the hypens and capitalize <code>json_payload</code> to <code>JsonPayload</code>
-- lower the character around a number Json_2_Lua to <code>Json2lua</code> hence <code>P2P</code> to <code>p2p</code>
-- note that an already camelized label can still be modified
-- eg <code>JSONPayload</code> to  <code>JsonPayload</code>
-- but also that the first letter is not capitelized neither @see Write.change_first
-- @tparam string label the identifier
-- @treturn string the camelized label
function Writer:camelize(label)
    return label:gsub('_(.)', function(a) return a:upper() end)
                :gsub('([A-Z])([A-Z]+)', function(a,b) return a..b:lower() end)
                :gsub('(%d)([A-Z])', function(a,b) return a..b:lower() end)
                :gsub('([A-Z])(%d)', function(a,b) return a:lower()..b end)
end

--- hyphenize an identifier 
-- add the hypens between numbers or camel boundaries and lower the identifier 
-- eg <code>JsonPayload2luaPayload</code> to <code>json_payload_2_lua_payload</code>
-- @tparam string label the identifier
-- @treturn string the camelized label
function Writer:hyphenize(label)
    return label:gsub('([^A-Z])([A-Z])', '%1_%2')
                :gsub('([^0-9_])([0-9])', '%1_%2')
                :gsub('([0-9])([^0-9_])', '%1_%2')
                :lower()
end

--- upper or lower an identifier first letter
-- add the hypens between numbers or camel boundaries and lower the identifier 
-- eg <code>JsonPayload2luaPayload</code> to <code>json_payload_2_lua_payload</code>
-- @tparam string label the identifier
-- @tparam function to string.upper or string.lower
-- @treturn string the camelized label
function Writer:change_first(label, to)
    return string[to](label:sub(1,1))..label:sub(2)
end

--- apply a method on a node vector or a function or a text vector
-- simpler than a reduce 
-- @param  t the node vector or text vector
-- @param fct is either a <code>function(e,Writer,sep) return sep' end</code> or
--            a <code>string</string> that shall be a methode name in the
-- @tparam string sep a 
-- @return self
function Writer:apply(t, fct, sep)
    sep = sep or ''
    for i=1,#t do 
        if type(fct) == 'function' then
            sep = fct(t[i], self, sep)
        else
            sep = t[i][fct](t[i], self, sep) 
        end
    end
    return self
end

--- Constructor
-- @tparam OutStream stream on which the Writer will write, if nil, will use
--                   a io.stdout based stream
-- @tparam number step optional number of spaces to use for the indentation. If 
--                nil
-- @treturn Writer a Writer instance
function Writer:__call(stream, step)
    -- could be assert(getmetatable(stream) == OutStream) if OutStream final.
    stream = stream or OutStream() 
    assert(type(stream) == 'table' and stream.write and stream:is_ok(), 'invalid stream')
    step   = step or '\t'
    
    local writer = {
        context = {},
        indentation = 0,
        indentation_step = step == '\t' and step or rep(' ', step),
        rep = string.rep, -- bof bof
        stream = stream,
        lastline = '',
        offset = 0,
        vars = {}
	}
	
	setmetatable(writer, self)
	
	return writer
end

--- get the stream associated with
-- can be used to retrieve the string that is being built with a string stream
-- <code>tostring(writer:get_stream())</code>
-- or to close the stream
-- @see OutStream
-- @treturn OutStream the target stream of the writer
function Writer:get_stream()
	return self.stream
end

-- make it a final class
setmetatable(Writer, Writer)
Writer.__index = Writer

return Writer
